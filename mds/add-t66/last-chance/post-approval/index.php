<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Make It Easier On Yourself</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=4">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=4">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_GET['cbur'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
});
</script>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="web/i/nt-upsell-banner-new.png?v=2">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1>Make It Easier On Yourself</h1>

        <div class="insertion left">
            <center><img class="mobile-half-width" src="web/i/diabet-cover-main.jpg" width="185" alt=""></center>
        </div>

        <p>You grabbed <b><i>The Truth About Diabetes and ED</i></b>.</p>
        <p>Yet you passed on getting a bottle of <b>T-66</b>.</p>
        <p>I’ll be quick.</p>
        <p>T-66 will not only boost your chances of beating type 2 diabetes, but it will also give you plenty of juice to help knock out ED.</p>
        <p>As you learned on the last page, men with type 2 diabetes have a rare advantage over other men with ED. We know the cause of the issues and the answers to turn it around.</p>
        <p>Well, one of those answers to beat ED is to boost your levels of Testosterone.</p>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="web/i/T-66_rendering-1bottle.png" width="135" alt=""></center>
        </div>

        <p><b><i>T-66 is shown to crank up your Testosterone levels 400% greater than compared to a placebo.</i></b></p>
        <p>Which means, if you grab a bottle of T-66 you will be on the fast track to beat both type 2 diabetes and ED.</p>
        <p>Look, type 2 diabetes robs you of living like the man you want. It cripples your health, it castrates your libido, and it adds stress.</p>
        <p>Right now, you can get everything you need to knock out all of these nasty effects. Grabbing a bottle of T-66 is a fast, and PROVEN way to give you an added boost.</p>
        <p>Be fair to yourself, and try a bottle.</p>

        <p align="center"><a href="http://m-s4.mdsbook.pay.clickbank.net/?cbur=a"><img class="mobile-full-width" src="web/i/add-to-order-with-price.jpg" width="320" alt=""></a></p>

        <hr>

        <p class="aux ta-center"><i>No thanks. <a href="http://m-s4.mdsbook.pay.clickbank.net/?cbur=d">I’ll pass on making this easier.</a></i></p>


    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>Prior to using these products or information, take them to your physician for approval. These statements have not been evaluated by the Food and Drug Administration. This product is not intended to diagnose, treat, cure, or prevent any disease. Please Note: The material on this site is provided for informational purposes only and is not medical advice. Always consult your physician before beginning any diet or exercise program. </p>
    <p>
        Copyright <?=date('Y')?> &copy; Nutrathesis.com<br>
        For support please contact support [at] nutrathesis.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
