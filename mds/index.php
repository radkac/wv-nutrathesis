<?php
include("../lib/Browser/Browser.php");
$browser = new Browser();
$mobile = $browser->isMobile() || $browser->isTablet();

$vtidBase = "nuvsl0808d";
$vtid = $mobile ? "mo".$vtidBase : $vtidBase;

$buyLink = "http://m-1.mdsbook.pay.clickbank.net/?cbfid=34354&vtid=" . $vtid;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Male Diabetes Solution</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=11">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=11">

    <!-- jQuery -->
    <script src="web/s/jquery.js"></script>

    <!-- Vimeo API -->
    <script src="https://player.vimeo.com/api/player.js"></script>

    <!-- bxSlider -->
    <link rel="stylesheet" href="web/s/jquery.bxslider.css?v=2">
    <script src="web/s/jquery.bxslider.js?v=2"></script>

<script type="text/javascript">

$(function(){

<? if(isset($_REQUEST['nodelay'])): ?>
    $('.delayed').show();
<? else: ?>
    setTimeout(function(){ $('.delayed').fadeIn(1200) }, 1045000); // 17:25=1045sec
<? endif; ?>

    var iframe = document.getElementById('vimeoPlayer');
    var player = new Vimeo.Player(iframe);
    $('.unmute, .video .notification A').click(function(){
        player.setVolume(1);
        $('.unmutes').fadeOut(1000);
        $('.notification').addClass('passive');
        $("#play-video-div").hide();
    });
    var tgc = setInterval(function(){
        $('.unmute').toggleClass('highlight');
    }, 500);

    $('.tmslider').bxSlider({
        auto: true,
        pause: 8000,
    });

    $("#play-video-div").click(function () {
        player.setVolume(1);
        $('.unmutes').fadeOut(1000);
        $('.notification').addClass('passive');

        $("#play-video-div").hide();
    });
});

</script>

<?php include "../tracking/ga-tracking.php"; ?>

    <!-- Hotjar Tracking Code for ESE -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:923516,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<body>

<div class="cdate"><?=date('l, F j, Y')?></div>

<header>
    <section>

        <div class="insertion center logo">
            <center><img src="web/i/nutrathesis.png?v=2" width="215px" alt=""></center>
        </div>

    </section>
</header>

<main>
    <section>

        <h1 class="first">Episode One: Male Diabetes <br class="desktop-hidden"><span class="ctrl-toggle"><b>Video</b><a href="/mds/article/" title="Click to see video">Article</a></span></h1>

        <div class="video">
            <p class="notification">
                <a href="javascript:;">make sure your sound is turned on</a>
            </p>
            <div class="player">
                <div id="play-video-div" style="width: 100%;height: 100%;position: absolute;top: 0;left: 0;z-index: 10000;"></div>
                <iframe id="vimeoPlayer" src="//player.vimeo.com/video/278828335?autoplay=1&title=0&byline=0&portrait=0&muted=1" width="760" height="427" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
            <div class="unmutes">
                <a class="unmute lt" href="javascript:;">Click to turn on sound</a>
                <a class="unmute rt" href="javascript:;">Click to turn on sound</a>
            </div>
        </div>

<div class="hidden delayed">

        <div class="insertion center" style="margin: 1.5em 0 0;">
            <center><img class="mobile-full-width" src="web/i/dfacts.jpg" width="760px" alt=""></center>
        </div>

        <div class="insertion center" style="margin: 1.5em 0 0;">
            <center><img class="mobile-full-width" src="web/i/cover-bundle.jpg?v=2" width="760px" alt=""></center>
        </div>

        <p class="price">$37</p>

        <div class="pbutton">
            <a href="<?php echo $buyLink; ?>1" data-id="1" class="button-buy-link">Add To Cart</a>
        </div>

        <p>The Male Diabetes Solution is the first and only formula for men looking to beat type 2 diabetes. It's a complete start-to-finish playbook. One tailored to boost Testosterone levels to get rid of male type 2 diabetes. And get rid of it &mdash; *FAST.*</p>
        <p>I've put over ten years of hard work into The Male Diabetes Solution. I made sure this one-of-a-kind playbook fit the needs of a man facing type 2 diabetes. For men, there is no other guide like it. It's dependable, it's simple, it gets the job done. Period.</p>
        <p>Here's a taste of what you'll find inside The Male Diabetes Solution.</p>

        <ul class="disk blue">
            <li><p>The one thing a man must do first thing in the morning to trigger a surge in natural Testosterone levels.</p></li>
            <li><p>Despite widespread belief why eating right after a common physical activity packs on belly fat.</p></li>
            <li><p>STOP Weighing yourself! How stepping on the weight scale demolishes your Testosterone levels and leads to stubborn fat gain. Know the ONE measurement to concern yourself with.</p></li>
            <li><p>Chocolate, coffee, and beer. How they can be your iron fortress against type 2 diabetes.</p></li>
            <li><p>How to talk to your Doctor to get the latest and best solutions to beat type 2 diabetes.</p></li>
            <li><p>The 15% eating rule &mdash; cut out 15% of this and blast off 15 pounds of stubborn belly fat in just a few short weeks.</p></li>
            <li><p>The RIGHT kind of physical activity tailored to your lifestyle which can reverse your type 2 diabetes.</p></li>
            <li><p>How to know if you need your Testosterone Levels checked (Hint: It’s based on the last time your wife begged you for sex).</p></li>
            <li><p>If you don’t have time to work out &mdash; there is an odd but simple way to maximize your natural Testosterone levels.</p></li>
        </ul>

        <p>And more. So much more.</p>
        <p>It all boils down to this.</p>
        <p>The Male Diabetes Solution is for the man who wants dependable answers. A man who likes predictable results.</p>
        <p>It's for the man frustrated by the time and effort it takes just to manage this disease. And it's for the man tired of the high emotional cost from type 2 diabetes.</p>
        <p>You can get your hands on this playbook for only $37.</p>

        <div class="pbutton">
            <a href="<?php echo $buyLink; ?>2" data-id="2" class="button-buy-link">Add To Cart</a>
        </div>

        <p>What's inside are solutions directed to men who care about solutions.</p>
        <p>These solutions can matter, because practical men are affected, directly or indirectly by the world of good, sound, and proven advice.</p>
        <p>So, For $37, if you don’t find that a fair and reasonable price for an intelligent solution to beat type 2 diabetes, then we are not a fit.</p>
        <p>I want to get the Male Diabetes Solution in the hands of as many men as I can. I also am keen on the fact we don’t know each other. You have no reason to believe me right now.</p>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="web/i/guarantee-seal.png" width="220" alt=""></center>
        </div>

        <p>So, I’m going to guarantee your money for 60 Days.</p>
        <p>I want you to see for yourself, firsthand, just how easy it is for you to reverse or beat type 2 diabetes.</p>
        <p>You can take 60 Days and be the judge yourself.</p>
        <p>Take 60 days and experience firsthand what it’s like to:</p>

        <ul class="check green">
            <li><p>Instantly boost your testosterone levels to reverse type 2 diabetes.</p></li>
            <li><p>Blast off your excess belly fat crippling your health.</p></li>
            <li><p>Enjoy the freedom, vitality, and confidence of a supercharged libido.</p></li>
            <li><p>Get the fast-track playbook to be free of this disease.</p></li>
            <li><p>Reclaim your life, freedom, and masculinity.</p></li>
        </ul>

        <p>And at the end of 60 Days, if for whatever reason you do not like the Male Diabetes Solution, let me know, and I will give you a prompt and courteous refund. No questions asked. If we’re not a fit for each other, we are not a fit.</p>
        <p>It doesn’t matter the reason.</p>
        <p>No matter how skeptical you are, you get 60-risk-free-days to find out for yourself if the Male Diabetes Solution is your answer.</p>
        <p>All you have to do is press the button below right now and claim your copy.</p>

        <p align="center">Try It Risk FREE for 60 Days</p>

        <div class="pbutton">
            <a href="<?php echo $buyLink; ?>3" data-id="3" class="button-buy-link">Add To Cart</a>
        </div>

        <p>Press the button, fill out your information, and then the Male Diabetes Solution will be delivered to you instantly.</p>
        <p>But, you’re not just getting the Male Diabetes Solution today.</p>
        <p>You are also getting both of these Diabetes busting weapons today, absolutely FREE.</p>

        <h2>FREE Diabetes-Destroying Gift #1</h2>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="web/i/ecover-mealplans-3d.jpg" width="240" alt=""></center>
        </div>

        <p>The first weapon you get in your arsenal, are the Male Diabetes Meal Plans.</p>
        <p>These are guy-friendly, Testosterone cranking meal plans. They are backed by the best nutritional science and give you a nutritional arsenal to force type 2 diabetes into surrender &mdash; FAST.</p>
        <p>Best of all, these meals are easy to prepare. I made them so even my Dad could have whipped up them up. My Dad burned toast, couldn’t even make pasta, and barely remembered where the knives were in the kitchen.</p>
        <p>I ensured the meals are almost done for you. Just get the foods, look at the simple steps, and it’s as simple as set it and forget it. In just a matter of minutes, you will have a meal which satisfies a man’s hunger while also cranking up your Testosterone.</p>

        <h2>FREE Diabetes-Destroying Gift #2</h2>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="web/i/ecover-workout-3d.jpg" width="240" alt=""></center>
        </div>

        <p>The second weapon you’re getting today is optional. However, I figure it’s not the worst idea in the world to give you one of the fastest ways to obliterate your type 2 diabetes.</p>
        <p>It’s the Male Diabetes Solution Workout Plan.</p>
        <p>Now, before you let your notions jump to conclusions, these workouts are the opposite of some sweating, grinding, bodybuilding workout.</p>
        <p>Instead, you get a simple, doable, and enjoyable workout guide &mdash; full of unique and easy strategies &mdash; to boost your Testosterone. These workouts are designed to work with your lifestyle, no matter your hectic work schedule or how much you think exercise is a chore.</p>

        <div class="pbutton">
            <a href="<?php echo $buyLink; ?>4" data-id="4" class="button-buy-link">Add To Cart</a>
        </div>

        <p>Today &mdash; I want nothing to get in the way of your decision.</p>
        <p>So, I’m going to sweeten the pot just a bit more.</p>
        <p>I’m also going to include three more guides to help you beat type 2 diabetes.</p>

        <h2>Free SUPER-BONUS #1</h2>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="web/i/ecover-anti-estrogen-handbook.jpg" width="240" alt=""></center>
        </div>

        <p>When you get the Male Diabetes Solution &mdash; I’m also going to include my Anti-Estrogen Handbook.</p>
        <p>Right now, in your house, at your work, and in your daily life, are covert items which spike your Estrogen.</p>
        <p>And for men, Estrogen elicits effeminate-like emotional reactions, generates “man-boobs,” and undermines your sexual competence.</p>
        <p>Furthermore, these covert items make beating type 2 diabetes an utter battle.</p>
        <p>So, instead of being a patsy to Estrogen - you’ll have my top tips, secrets, and insights on the foods to eat, the items to eliminate in your house immediately - and the soaps you MUST stop using at all costs.  And more &mdash; a lot more!</p>
        <p>You’ll Estrogen Proof your life, and enjoy a faster path to beating male type 2 diabetes.</p>
        <p>Usually, this handbook is a $27 value, but it’s yours FREE today with your purchase of the Male Diabetes Solution.</p>

        <h2>Free SUPER-BONUS #2</h2>

        <div class="insertion right">
            <center><img class="mobile-3of4-width" src="web/i/ecover-advocate.jpg" width="300" alt=""></center>
        </div>

        <p>I’m also giving you 15 Days to test out my premium email coaching service called The Advocate.</p>
        <p>Inside the Advocate, it’s like having me right next to you as your coach.</p>
        <p>Two to three times per week I’ll send you a personally crafted male-specific health tip from me.</p>
        <p>I inspect the cutting edge science you need to make sure you get your best results. You’ll start each day on the right path, and fast-track your way to peak masculine health.</p>
        <p>Advocate is exclusive to my top clients, and for 15 days you get behind the scenes access.</p>
        <p>Typically, it costs $17 just for 15 days. However, you get it today as part of your Male Diabetes Solution package.</p>
        <p>PLUS if you decide to stay in my email coaching, you’ll get the preferred monthly rate of just $9 instead of the regular subscription cost!</p>
        <p>I can’t offer that to everyone though so make sure you take advantage while you can.</p>
        <p>I’m also going to include another excellent bonus.</p>

        <h2>Free SUPER-BONUS #3</h2>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="web/i/ecover-nutrition-calculator.jpg" width="240" alt=""></center>
        </div>

        <p>I want nothing &mdash; I mean absolutely nothing &mdash; to get in your way of success. So, I’m tossing in today as part of your Male Diabetes Solution Package &mdash; The Diabetes Solution Diet Calculator.</p>
        <p>The Diabetes Solution Diet Calculator makes how much to eat, when to eat, or even what to eat &mdash; all done for you.</p>
        <p>I know when you think calculator, you think math. Well, the math is done for you.</p>
        <p>All you have to do is plug in 6 simple body measurements, and the rest is done.</p>
        <p>And yes, you know these numbers already. Just plug in the numbers, and you get a 7-day map of what to eat.</p>
        <p>It works no matter your lifestyle, busy day, or what kinds of foods you love.</p>
        <p>With this calculator, you will peel off belly flab day in and day out. Which, as you know, will boost your Testosterone levels while eradicating type 2 diabetes from your body.</p>
        <p>The calculator &mdash; to get such precise numbers &mdash; on its own could cost $97. However, today, it’s yours free when you buy Male Diabetes Solution.</p>
        <p>All of these fantastic bonuses are included in the 60 Day Money-back-guarantee.</p>
        <p>Just click the button below now to get The Male Diabetes Solution along with all your super-bonuses.</p>

        <div class="insertion center" style="margin: 1.5em 0 0;">
            <center><img class="mobile-full-width" src="web/i/cover-bundle.jpg?v=2" width="760px" alt=""></center>
        </div>

        <div class="pbutton">
            <a href="<?php echo $buyLink; ?>5" data-id="5" class="button-buy-link">Add To Cart</a>
        </div>

</div><!-- .hidden.delayed -->

    </section>
</main>

<footer>
    <section>

        <hr>
        <p class="ta-left">*Results may vary</p>
        <p class="ta-justify">ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales, Inc., a Delaware corporation located at 1444 S. Entertainment Ave., Suite 410 Boise, ID 83709, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products. The Content on the website is intended for educational and informational purposes only. The Content is not intended to provide medical advice and, to the extent that medical advice is required, users should consult with qualified medical professionals. Information in text files, interactive tools, messages or articles on this website cannot replace consultations with qualified health-care professionals to meet your individual medical needs.</p>
        <p class="ta-center">
            For any questions, please contact us at support [at] nutrathesis.com<br>
            Copyright &copy; 1994-<?=date('Y')?> by NutraThesis.com. All Rights Reserved.<br>
            Our <a href="/privacy/">Privacy Policy</a> &nbsp;&amp;&nbsp;
            <a href="/terms/">Terms &amp; Conditions</a>.
        </p>

    </section>
</footer>


<!-- This site is converting visitors into subscribers and customers with OptinMonster - https://optinmonster.com -->
<script type="text/javascript" src="https://a.optmstr.com/app/js/api.min.js" data-campaign="fd3gqf3kiogqpioc0ic5" data-user="48417" async></script>
<!-- / https://optinmonster.com -->


<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

<script>
    $(".button-buy-link").on('click', function () {
        var id = $(this).data('id');

        gtag('event', 'click', {
            'event_category' : '0808d-buy-button-' + id,
            'event_label' : 'default'
        });
    });
</script>
</body>
</html>
