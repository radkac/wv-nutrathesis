<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Free Advocate Trial</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=3">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=3">

</head>
<body>

<div id="layoutcb" class="clearfix">

    <div id="content" class="clearfix">
        <h1>Account Setup Step 1:</h1>
        <div id="warning">Warning: Leaving The Page Or Hitting The Back Button Will Cause Your Account Setup To Fail</div>
        <h2 align="center"><b>Please Confirm Your VIP Newsletter Access</b></h2>

        <p>Included with your <i>Male Diabetes Solution</i> purchase today is 21 days of FREE access to my premium VIP Newsletter service called the Advocate. You have also secured your subscription at the discounted price of only $9 a month recurring once your free 21 day trial ends.</p>
        <p>However, this free offer is only available to a limited number of customers. So before we complete your order I need to make sure you want to keep your premium access.</p>
        <p>The Advocate is an <b>exclusive VIP</b> subscription service giving you the ultimate insider’s look at all of my latest weight loss, fitness and health enhancing advice based on my ongoing scientific research into nutrition, exercise, weight loss and health.</p>
        <p>It’s where we bust the mainstream nutrition myths and uncover the most up-to-date <u>proven strategies you can apply immediately</u> to lose weight, maximize your energy and improve your health as quickly as possible.</p>
        <p>You can see why your free access is so valuable. And why we need to make sure you are 100% committed to keeping your spot.</p>
        <br>
        <p align="center"><a id="accept" class="button-cancel" href="http://m-2.mdsbook.pay.clickbank.net/?cbur=a&cbrblaccpt=true">Add to my Order</a></p>
        <br>
        <hr>

        <p>I want to give away my free access to the VIP Advocate Newsletter. I understand this is my only chance to get free access to this insider information and secure the discounted price of only 9 dollars a month. <a href="http://m-2.mdsbook.pay.clickbank.net/?cbur=d">Cancel my access.</a></p>

        <hr>

        <p>ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales Inc., a Delaware corporation located at 1444 S. Entertainment Ave., Suite 410 Boise, ID 83709, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement oropinion used in promotion of these products.</p>

    </div><!-- #content -->

</div><!-- #layout -->

</body>
</html>
