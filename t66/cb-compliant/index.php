<!DOCTYPE html>
<html lang="en">
<head>
    <title>T-66</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=7">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=7">

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script>
$(function(){

    // Options
    $('.packages .column').click(function(){
        var e = $(this);
        if(e.hasClass('option-1')) {
            window.location.href = 'http://m-s5.mdsbook.pay.clickbank.net/?cbfid=34355&vtid=specialoffer';
        } else if(e.hasClass('option-2')) {
            window.location.href = 'http://m-s6.mdsbook.pay.clickbank.net/?cbfid=34355&vtid=specialoffer';
        } else if(e.hasClass('option-3')) {
            window.location.href = 'http://m-s7.mdsbook.pay.clickbank.net/?cbfid=34355&vtid=specialoffer';
        }
    });

    // Scroll to options from bottom
    $(".payline .purch A").click(function() {
        $('html, body').animate({
            scrollTop: $(".options").offset().top
        }, 2000);
    });

    // Open popup
    $('.labelPopupShow').click(function(){
        var bodyWidth = $("BODY").width();
        popupWidth = (bodyWidth > 1020 ? 1020 : bodyWidth-20);
        $("#popupLabelDialog" ).dialog({
            closeOnEscape: true,
            //dialogClass: 'noTitleStuff',
            draggable: false,
            modal: true,
            resizable: false,
            width: popupWidth
        });
    });

});
</script>

</head>
<body>

<header>
    <section>

        <div class="banner">
            <div class="columns">
                <div class="column cl-1-4 ta-center">
                    <img class="mobile-1of3-width bottle" src="web/i/tcycle-prime-1-bottle.png" width="80%" alt=""><br>
                    <img class="mobile-1of3-width price" src="web/i/tcycle-prime-1-bottle-price.png?v=2" width="90%" alt="">
                </div>
                <div class="column cl-3-4 ta-center">
                    <h4 class="spec-1">
                        SPECIAL OFFER<br>
                        <span class="bg-yellow marker">UNADVERTISED</span> DISCOUNT
                    </h4>
                    <h4 class="spec-2">
                        SPECIAL OFFER<br>
                        SAVE UP TO 48% PER BOTTLE
                    </h4>
                    <p class="hurry">BUT HURRY, THIS UNADVERTISED DISCOUNT IS NOT GUARANTEED BEYOND TODAY!</p>
                    <div class="boxed">
                        <h5>
                            GET UP TO $38 OFF PER BOTTLE TODAY BY<br>
                            <span class="cl-red">STOCKING UP FOR MAXIMUM SAVINGS!</span>
                        </h5>
                        <p>YOU ARE ALWAYS PROTECTED BY OUR <span class="bg-yellow marker">60 DAY MONEY BACK GUARANTEE</span></p>
                        <div class="seal"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footnote">*Additional discounts are only guaranteed when ordering today.</div>

    </section>
</header>

<main class="default options">
    <section>

        <h2 class="cl-gray">CHOOSE YOUR PACKAGE</h2>

        <div class="columns packages">
            <div class="column cl-1-3 option-1">
                <div class="status regular"></div>
                <div class="prodbox">
                    <img class="mobile-3of4-width visual" src="web/i/bottle-1.png" width="90%" alt=""><br>
                    <div class="price"><sup>$</sup>59</div>
                    <div class="descr">
                        One Bottle<hr>
                        <strike>&nbsp;$97&nbsp;</strike> <em>SAVE $38</em>
                    </div>
                    <p class="purch"><a href="javascript:;">ADD TO CART</a></p>
                    <div class="badge"></div>
                </div>
                <div class="shadow"></div>
            </div>
            <div class="column cl-1-3 option-2">
                <div class="status mostpop">MOST POPULAR</div>
                <div class="prodbox">
                    <img class="mobile-3of4-width visual" src="web/i/bottle-3.png" width="90%" alt=""><br>
                    <p class="price"><sup>$</sup>147</p>
                    <div class="descr">
                        Three Bottles<hr>
                        <strike>&nbsp;$291&nbsp;</strike> <em>SAVE $144!</em>
                    </div>
                    <p class="purch"><a href="javascript:;">ADD TO CART</a></p>
                    <div class="badge"></div>
                </div>
                <div class="shadow"></div>
            </div>
            <div class="column cl-1-3 option-3">
                <div class="status regular"></div>
                <div class="prodbox">
                    <img class="mobile-3of4-width visual" src="web/i/bottle-6.png" width="90%" alt=""><br>
                    <p class="price"><sup>$</sup>243</p>
                    <div class="descr">
                        Six Bottles<hr>
                        <strike>&nbsp;$582&nbsp;</strike> <em>SAVE $339!</em>
                    </div>
                    <p class="purch"><a href="javascript:;">ADD TO CART</a></p>
                    <div class="badge"></div>
                </div>
                <div class="shadow"></div>
            </div>
        </div>

        <div class="features">
            <span>
                <img src="web/i/features-icon.png" width="20px" alt="">
                60 Day Money Back Guarantee
            </span>
            <span>
                <img src="web/i/features-icon.png" width="20px" alt="">
                100% Secure Order
            </span>
        </div>

    </section>
</main>

<main class="grayed">
    <section>

        <h3><b>Frequently Asked Questions</b></h3>

        <dl>
            <dt><p>Q: Who formulated the T-66 Super Supplement?</p></dt>
            <dd>
                <p>T-66 was formulated by Brad Pilon.</p>
                <p>Brad Pilon is a 3 time, best-selling weight loss author. He has a degree in Human Biology and Nutrition from the University of Guelph, as well as a Master’s degree in Applied Human Nutrition. He has extensive experience working in the health and supplement industry in R&amp;D for both fat loss and muscle building. He has spent the past 15 years researching and developing cutting edge weight management solutions for men.</p>
            </dd>
            <dt><p>Q: How much should I order?</p></dt>
            <dd>
                <p>The most popular choice is the 3 and 6 bottle options. The 3-bottle option is also recommended for those who are looking to drop at least 10 or more pounds of fat and wanting to experience all the benefits attainable when your natural testosterone levels are maximized. This option is often a great place to start because you can experience all the benefits T-66 can offer while ensuring you have ample supply on hand without the need to reorder often. However, if you consider yourself a “trouble case” &mdash; looking to lose significantly more than ten pounds, or you’ve been plagued with a protruding belly for a long time &mdash; the 6-bottle option is very popular and highly recommended. Remember, you can always return what you don’t use if you achieve results faster than anticipated and any either of these discounted packages offered today will allow you to take advantage of our lowest possible prices.</p>
            </dd>
            <dt><p>Q: Are these supplements vegetarian friendly?</p></dt>
            <dd><p>Yes.</p></dd>
            <dt><p>Q: What support do you offer clients of T-66?</p></dt>
            <dd>
                <p>A very important part of T-66 is the customer support you'll receive. We offer you 24/7 customer support via our email help desk at support@nutrathesis.com.</p>
            </dd>
            <dt><p>Q: Why is the T-66 such a unique and powerful testosterone boosting solution for MEN?</p></dt>
            <dd>
                <p>Unlike many testosterone supplements, the formulation in T-66 was actually researched and tested on men. Unfortunately, many mainstream testosterone supplements on the market do not contain clinically backed or research proven ingredients because of the cost. Also, these mainstream supplements are usually only tested on mice at best, not actual men. We do not compromise ingredients or cut their effectiveness for cost reasons. With each serving of T-66 you’re getting the clinically tested formulation at the effective proven dose.</p>
            </dd>
            <dt><p>Q: Is T-66 a drug?</p></dt>
            <dd>
                <p>No. T-66 is formulated with 100% safe and natural ingredients clinically proven to increase a man’s natural production of testosterone. It is not an illegal pro-hormone or steroid of any kind, and does not contain any drugs.</p>
            </dd>
            <dt><p>Q: What’s in the T-66 Supplement?</p></dt>
            <dd>
                <p><a class="labelPopupShow" href="javascript:;">Click here</a> to see the T-66 label.</p>
            </dd>
            <dt><p>Q: What’s the best way to take T-66?</p></dt>
            <dd><p>Simply take 2 capsules twice per day.</p></dd>
            <dt><p>Q: How long does 1 bottle of T-66 last for?</p></dt>
            <dd><p>Each bottle contains a full 30-day supply. This is why our most discerning customers and those most serious about experiencing noticeable, long term results choose the 3 or 6 bottle option.</p></dd>
            <dt><p>Q: What if I have a medical condition?</p></dt>
            <dd><p>It's always recommended to consult a medical professional before beginning any supplement, diet or exercise plan.</p></dd>
            <dt><p>Q: How long will shipping take?</p></dt>
            <dd><p>North American customers can expect their orders within 5 to 7 business days. Orders will be shipped out on the next business day. You will be emailed a tracking number when they ship.</p></dd>
            <dt><p>Q: What if this product doesn't work?</p></dt>
            <dd><p>If you are unsatisfied with your purchase, just let us know and we will issue you a prompt and courteous refund even on empty bottles that were taken as directed. You are always protected by our iron-clad 60-Day <b>100% Money Back Guarantee</b>.</p></dd>
            <dt><p>Q: What about refunds?</p></dt>
            <dd><p>Refund requests are accepted within 60 days of order date. If you select to receive physical items, all items must be returned to  6000 Pardee, Taylor, MI 48180 undamaged along with your full name, email address, Clickbank email invoice, and order number. Returned items and return shipping costs are your responsibility. Please keep a record of your return tracking. A refund will be issued once we receive returned items. Any items that are damaged when we receive them will not be eligible for refund. Refunds do not include any shipping or handling charges.</p></dd>
        </dl>

    </section>
</main>

<main class="payline">
    <section>

        <div class="columns">
            <div class="column cl-1-5 ta-center">&nbsp;</div>
            <div class="column cl-1-5 ta-center">
                <img class="mobile-1of3-width" src="web/i/60-day-badge.png" width="70%" alt="">
            </div>
            <div class="column cl-2-5 ta-center">
                <p class="purch"><a href="javascript:;">ADD TO CART</a></p>
                <p>MAKE YOUR SELECTION</p>
            </div>
            <div class="column cl-1-5 ta-center">&nbsp;</div>
        </div>

    </section>
</main>

<main class="grayed simple">
    <section>

        <p class="ta-center"><img class="mobile-3of4-width" src="web/i/money-back-guarentee.jpg" width="360px" alt=""></p>
        <p class="proofers">
            <img class="mobile-half-width" src="web/i/godaddy.png" width="200px" alt="">
            <img class="mobile-half-width" src="web/i/pay-logos-stacked.png" width="200px" alt="">
            <img class="mobile-half-width" src="web/i/bbb.png" width="170px" alt="">
        </p>

        <p class="foot-1">*These statements in this video have not been evaluated by the Food and Drug Administration. The information in this document is by no way intended as medical advice or as a substitute for medical counselling. Consult your physician before following any information presented in this video presentation. The product recommended and information presented are not intended to treat, diagnose, cure, or prevent any disease.<br>
        <br>
        ClickBank is the retailer of products on this site. CLICKBANK® is a registered trademark of Click Sales Inc., a Delaware corporation located at 1444 S. Entertainment Ave., Suite 410 Boise, ID 83709, USA and used by permission. ClickBank's role as retailer does not constitute an endorsement, approval or review of these products or any claim, statement or opinion used in promotion of these products.</p>
        <p class="foot-2"><small>Copyright &copy; <?=date('Y')?> All Rights Reserved. nutrathesis.com</small></p>

    </section>
</main>

<div id="popupLabelDialog" title="T-66 label">
    <p align="center"><img src="web/i/tcycle-label.jpg" width="100%" alt=""></p>
</div>

<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
