<?php

/**
 * Returns VTID tailored to specific upsell. Logic is the following:
 * - if it is first upsell - just append, don't remove
 * - if it is not first upsell - remove last 2 characters of the VTID (identifier of previous upsell) and append new identifier
 * @param $vtid string Passed VTID
 * @param $vtidAppend string VTID identifier for current upsell
 * @param $isFirstUpsell bool Is current upsell first or no?
 * @return string New VTID
 */
function resolveVTID($vtid, $vtidAppend, $isFirstUpsell) {
    if(!$isFirstUpsell && strlen($vtid) >= 2)
        $vtid = substr($vtid, 0, strlen($vtid)-2);

    return $vtid.$vtidAppend;
}


/**
 * Returns the Clickbank buy link - most general function
 * @param $productId string|null
 * @param $affiliate string|null
 * @param $vendor string
 * @param $params array Array of query params
 * @return string
 */
function getClickbankBuyLink($productId, $affiliate, $vendor, $params)
{
    $productId = $productId ? $productId."." : "";
    $affiliate = $affiliate ? $affiliate."_" : "";
    $query = http_build_query($params);

    return "http://{$productId}{$affiliate}{$vendor}.pay.clickbank.net/?{$query}";
}

/**
 * Extracts the VTID from the URL. If there is a "passvtid" parameter present (because of affiliate offers),
 * return that, otherwise return regulat "vtid" parameter.
 * TODO - once hashing for parameters inside CB is enabled, it needs to be implemented here.
 * @return string
 */
function getVtid()
{
    if(isset($_GET['passvtid']))
        return $_GET['passvtid'];

    return isset($_GET['vtid']) ? $_GET['vtid'] : '';
}