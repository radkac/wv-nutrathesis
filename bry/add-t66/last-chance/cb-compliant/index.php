<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Make It Easier On Yourself</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=4">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=4">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>
function showWarning() { $('.warning-message').fadeTo('slow',1) }
function showContent() { $('#content').fadeTo('slow',1); $('#footer').fadeTo('slow',1); }
$(function(){
	// show warning message
	setTimeout('showWarning()', 1500); // 1500
	// show the rest of the content
	setTimeout('showContent()', 3000); // 3000
    // show all hidden items fast in case of "cbur" existence
    <?if(isset($_REQUEST['cbur']) or isset($_REQUEST['nodelay'])):?>
        $('.hidden').fadeTo('fast',1);
    <?endif;?>
});
</script>

</head>
<body>

<div id="header" class="no-deco clearfix">
    <div class="inner clearfix">

        <img class="mobile-full-width" src="web/i/nt-upsell-banner-new.png?v=2">

    </div><!-- .inner -->
</div><!-- #header -->

<div id="layout" class="clearfix">

    <div class="warning-message hidden">WARNING: DO NOT HIT YOUR "BACK" BUTTON. Pressing your "back" button can cause you to accidentally double-order</div>

    <div id="content" class="rich-text clearfix hidden">

        <h1>Make It Easier On Yourself</h1>

        <p>You grabbed <b><i>The Truth About Diabetes and ED</i></b>.</p>
        <p>Yet you passed on getting a bottle of <b>T-66</b>.</p>
        <p>I’ll be quick.</p>
        <p>We know the cause of the issues and the answers to turn it around.</p>

        <div class="insertion right">
            <center><img class="mobile-half-width" src="web/i/T-66_rendering-1bottle.png" width="135" alt=""></center>
        </div>

        <p><b><i>T-66 is shown to crank up your Testosterone levels 400% greater than compared to a placebo.</i></b></p>
        <p>Grabbing a bottle of T-66 is a fast, and PROVEN way to give you an added boost.</p>
        <p>Be fair to yourself, and try a bottle.</p>

        <p align="center"><a href="http://m-s4.mdsbook.pay.clickbank.net/?cbur=a"><img class="mobile-full-width" src="web/i/add-to-order-with-price.jpg" width="320" alt=""></a></p>

        <hr>

        <p class="ta-center">No thanks. <a href="http://m-s4.mdsbook.pay.clickbank.net/?cbur=d">I’ll pass on making this easier.</a></p>

        <hr>

        <p>Statements on this website have not been evaluated by the Food and Drug Administration. Products are not intended to diagnose, treat, cure or prevent any disease. If you are pregnant, nursing, taking medication, or have a medical condition, consult your physician before using our products.</p>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="hidden rich-text">

    <p>
        Copyright <?=date('Y')?> &copy; Nutrathesis.com<br>
        For support please contact support [at] nutrathesis.com<br>
    </p>

</div><!-- #footer -->
<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

</body>
</html>
