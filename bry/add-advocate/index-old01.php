<?php
require_once('../../lib/Tracking/upsell-analytics-vtid-logic.php');

$vtid = getVtid();
$vtid = resolveVTID($vtid, 'ad', true);
// CB upsell buy links
$linkAccept = getClickbankBuyLink('m-2', null, 'mdsbook', array(
    'cbur' => 'a',
    'vtid' => $vtid
));
$linkDecline = getClickbankBuyLink('m-2', null, 'mdsbook', array(
    'cbur' => 'd',
    'vtid' => $vtid
));

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Free Advocate Trial</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css?v=4">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css?v=4">
    <link rel="stylesheet" type="text/css" href="web/s/global.css?v=4">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css?v=4">

    <?php include "../../tracking/ga-tracking.php"; ?>
</head>
<body>

<div id="layout" class="clearfix">

    <div id="content" class="clearfix">
        <h1>Account Setup Step 1:</h1>
        <div id="warning">Warning: Leaving The Page Or Hitting The Back Button Will Cause Your Account Setup To Fail</div>
        <h2 align="center"><b>Please Confirm Your VIP Newsletter Access</b></h2>

        <p>Included with your <i>Male Diabetes Solution</i> purchase today is 15 days of FREE access to my premium VIP Newsletter service called the Advocate. You have also secured your subscription at the discounted price of only $9 a month recurring once your free 15 day trial ends.</p>
        <p>However, this free offer is only available to a limited number of customers. So before we complete your order I need to make sure you want to keep your premium access.</p>
        <p>The Advocate is an <b>exclusive VIP</b> subscription service giving you the ultimate insider’s look at all of my latest weight loss, fitness and health enhancing advice based on my ongoing scientific research into nutrition, exercise, weight loss and health.</p>
        <p>It’s where we bust the mainstream nutrition myths and uncover the most up-to-date <u>proven strategies you can apply immediately</u> to lose weight, maximize your energy and improve your health as quickly as possible.</p>
        <p>You can see why your free access is so valuable. And why we need to make sure you are 100% committed to keeping your spot.</p>
        <p>So if you’d like to cancel, just click 'cancel' below, otherwise to avoid losing your monthly membership and continue with your 15 day free trial, just click the <i>'No Thanks'</i> button below to protect your access and proceed with customizing your order.</p>

        <p align="center"><a id="accept" class="button-cancel" href="<?=$linkAccept?>&cbrblaccpt=true">No Thanks, Complete My Order</a></p>
        <br>
        <hr>

        <p>I want to give away my free access to the VIP Advocate Newsletter. I understand this is my only chance to get free access to this insider information and secure the discounted price of only 9 dollars a month. <a href="<?php echo $linkDecline; ?>">Cancel my access.</a></p>

    </div><!-- #content -->

</div><!-- #layout -->

<script src='//cbtb.clickbank.net/?vendor=eatstopeat'></script>

<iframe src="http://onlinedealtracking.com/path/frame.php?tid=&offerid=&amount=40&subid=" style="display:none;"></iframe>

</body>
</html>
