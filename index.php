<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="0; url=/mds/">
    <script type="text/javascript">
        window.location.href = "/mds/"
    </script>
    <title>Page Redirection</title>
</head>
<body>
    If you are not redirected automatically, follow this <a href='/mds/'>link</a>.
</body>
</html>
