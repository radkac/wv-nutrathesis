<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Privacy Policy</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

</head>
<body>

<div id="header" class="clearfix">

	<img class="mobile-half-width logo" src="web/i/head-logo.png" width="260px" alt="">

</div><!-- #header -->

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

        <h1>Privacy Policy</h1>

        <p>NutraThesis.com's Privacy Policy PLEASE NOTE: OUR PRIVACY POLICY CHANGES FROM TIME TO TIME AND CHANGES ARE EFFECTIVE UPON POSTING. PLEASE CHECK BACK FREQUENTLY FOR UPDATES AS IT IS YOUR SOLE RESPONSIBILITY TO BE AWARE OF CHANGES. NutraThesis DOES NOT PROVIDE NOTICES OF CHANGES IN ANY MANNER OTHER THAN BY POSTING THE CHANGES AT THIS WEB SITE.</p>
        <p>IF YOU DO NOT AGREE WITH THE TERMS OF THIS PRIVACY POLICY PLEASE DO NOT PROVIDE ANY INFORMATION TO NutraThesis OR USE ANY OF THE SERVICES OR PRODUCTS OFFERED OR PROVIDED ON ANY OF THE WEB SITES REFERRED TO IN THIS PRIVACY POLICY.</p>
        <p>The information collection, use, and dissemination practices of NutraThesis This policy applies to NutraThesis's collection, use, storage and disclosure of information by NutraThesis on its (a) websites, including all its divisions, subsidiaries and related companies (collectively, the "Websites"), (b) on various third party websites, and (c) to NutraThesis's other information collection, including the purchase of customer lists from third parties. NutraThesis is not responsible for the information collection or privacy practices of third web sites or applications.</p>

        <h2>1. Collection of Information.</h2>

        <h3>1.1 Survey Information.</h3>

        <p>NutraThesis collects information from individuals by various methods, including, but not limited to, when an individual voluntarily completes a NutraThesis survey, order form, or a registration pages either online or offline, or by means of online or offline surveys, order forms, or registration pages operated by third parties (collectively, a "Survey"). (As used herein, "online" means using the Internet, including the Websites, and related technologies, and "offline" means by methods other than online, including in person, in the postal mail, using telephones and cell phones, and other similar means.) In the Surveys, NutraThesis or a third party (a "Third Party") may ask an individual to provide various information to NutraThesis , which may include his or her name, email address, street address, zip code, telephone numbers (including cell phone numbers and carriers), birth date, gender, salary range, education and marital status, occupation, social security number, employment information, personal and online interests, and such other information as may be requested from time to time (together, "Survey Information"). NutraThesis may also collect information concerning an individual from another source and uses that information in combination with information provided from this web site. Completing the Surveys is completely voluntary, and individuals are under no obligation to provide Survey Information to NutraThesis or Third Party, but an individual may receive incentives from NutraThesis or a Third Party in exchange for providing Survey Information to NutraThesis.</p>

        <h3>1.2 Other Information.</h3>

        <p>Other occasions when NutraThesis obtains information from individuals include (1) when an individual is making a claim for a prize or seeking to redeem an incentive offered by NutraThesis or by a third party, (2) when an individual requests assistance through NutraThesis's customer service department, and (3) when an individual voluntarily subscribes to a NutraThesis service or newsletter (together, "Other Information").</p>

        <h3>1.3 Cookies, Web Beacons, and Other Info Collected Using Technology.</h3>

        <p>NutraThesis currently uses cookie and web beacon technology to associate certain Internet-related information about an individual with information about the individual in our database. Additionally, NutraThesis may use other new and evolving sources of information in the future (together, "Technology Information").</p>

        <h4>(a) Cookies.</h4>

        <!--<p>A cookie is a small amount of data stored on the hard drive of the individual's computer that allows NutraThesis to identify the individual with his or her corresponding data that resides in NutraThesis's database. You may read more about cookies at http://cookiecentral.com. Individuals who use the Websites need to accept cookies in order to use all of the features and functionality of the Websites.</p>-->

        <p id="cookiebot-declaration"></p>

        <h4>(a1) Google</h4>

        <p>We along with third-party vendors, such as Google use first-party cookies to compile data regarding user interactions with our website. It's also important to note that we allow third party behavioral tracking to serve ads based on a user’s prior visits to your website. This allows us to make special offers and continue to market our services to those who have shown interest in our service.</p>
        <p>We have implemented Demographics and Interests Reporting.</p>
        <p>Where users have chosen to enable Google to associate their web and app browsing history with their Google account, and to use information from their Google account to personalize ads they see across the web, Google will use data from its signed-in users together with Google Analytics data to build and define audience lists for cross-device remarketing. In order to support this feature, Google Analytics will collect these users’ Google-authenticated identifiers, which are Google’s personal data, and temporarily join them to anonymous Google Analytics data in order to populate advertising audiences audiences.</p>
        <p>We will not facilitate the merging of personally-identifiable information with non-personally identifiable information collected through any Google advertising product or feature unless we have robust notice of, and the user's prior affirmative consent to, that merger.</p>
        <p>You can opt out by visiting the Network Advertising initiative opt out page or using the Google Analytics Opt Out Browser add on. <a href="https://tools.google.com/dlpage/gaoptout/" target="blank">https://tools.google.com/dlpage/gaoptout/</a></p>

        <h4>(b) Web Beacons.</h4>

        <p>A web beacon is programming code that can be used to display an image on a web page (by using a programming function -- see http://truste.org for more information), but can also be used to transfer an individual's unique user identification (often in the form of a cookie) to a database and associate the individual with previously acquired information about an individual in a database. This allows NutraThesis to track certain web sites an individual visits online. Web beacons are used to determine products or services an individual may be interested in, and to track online behavioral habits for marketing purposes. For example, NutraThesis might place, with the consent of a third party website, a web beacon on the third party's website where fishing products are sold. When Bill, an individual listed in NutraThesis's database, visits the fishing website, NutraThesis receives notice by means of the web beacon that Bill visited the fishing site, and NutraThesis would then update Bill's profile with the information that Bill is interested in fishing. NutraThesis may thereafter prNutraThesisnt offers of fishing related products and services to Bill. In addition to using web beacons on web pages, NutraThesis also uses web beacons in email messages sent to individuals listed in NutraThesis's database.</p>

        <h4>(c) New Technology.</h4>

        <p>The use of technology on the Internet, including cookies and web beacons, is rapidly evolving, as is NutraThesis's use of new and evolving technology. As a result, NutraThesis strongly encourages individuals to revisit this policy for any updates regarding its use of technology.</p>

        <h3>1.4 Outside Information.</h3>

        <p>NutraThesis may receive information about individuals from third parties or from other sources of information outside of NutraThesis including information located in public databases ("Outside Information").</p>

        <h3>1.5 Individual Information.</h3>

        <p>As used herein, Individual Information means Survey Information, Third Party List Information, Other Information, Technology Information, and Outside Information, and any other information NutraThesis gathers or receives about individuals.</p>

        <h3>1.6 No Information Collected from Children.</h3>

        <p>NutraThesis will never knowingly collect any personal information about children under the age of 13. If NutraThesis obtains actual knowledge that it has collected personal information about a child under the age of 13, that information will be immediately deleted from our database. Because it does not collect such information, NutraThesis has no such information to use or to disclose to third parties. NutraThesis has designed this policy in order to comply with the Children's Online Privacy Protection Act ("COPPA").</p>

        <h3>1.7 Credit Card Information.</h3>

        <p>NutraThesis may in certain cases collect credit card numbers and related information, such as the expiration date of the card ("Credit Card Information") when an individual places an order from NutraThesis. When the Credit Card Information is submitted to NutraThesis, such information is encrypted and is protected with SSL encryption software. NutraThesis will use the Credit Card Information for purposes of processing and completing the purchase transaction, and the Credit Card Information will be disclosed to third parties only as necessary to complete the purchase transaction.</p>

        <h2>2. Use of Individual Information.</h2>

        <h3>2.1 Discretion to Use Information.</h3>

        <p>THE COMPANY MAY USE INDIVIDUAL INFORMATION FOR ANY LEGALLY PERMISSIBLE PURPOSE IN COMPANY'S SOLE DISCRETION. The following paragraphs in Section 2 describe how NutraThesis currently uses Individual Information, but NutraThesis may change or broaden its use at any time. As noted below, NutraThesis may update this policy from time to time. NutraThesis may use Individual Information to provide promotional offers to individuals by means of email advertising, telephone marketing, direct mail marketing, online banner advertising, and package stuffers, among other possible uses.</p>

        <h3>2.2 Email.</h3>

        <p>NutraThesis uses Individual Information to provide promotional offers by email to individuals. NutraThesis may maintain separate email lists for different purposes. If email recipients wish to end their email subscription from a particular list, they need to follow the instructions at the end of each email message to unsubscribe from the particular list.</p>

        <h4>2.2(a) Content of Email Messages.</h4>

        <p>In certain commercial email messages sent by NutraThesis, an advertiser's name will appear in the "From:" line but hitting the "Reply" button will cause a reply email to be sent to NutraThesis. The "Subject:" line of NutraThesis email messages will usually contain a line provided from the advertiser to NutraThesis.</p>

        <h4>2.2(b) Solicited Email.</h4>

        <p>NutraThesis only sends email to individuals who have agreed on the Websites to receive email from NutraThesis or to individuals who have agreed on third party websites to receive email from third parties such as NutraThesis. NutraThesis does not send unsolicited email messages. As a result, statutes requiring certain formatting for unsolicited email are not applicable to NutraThesis's email messages.</p>

        <h3>2.3 Targeted Advertising.</h3>

        <p>NutraThesis uses Individual Information to target advertising to an individual. When an individual is using the Internet, NutraThesis uses Technology Information (see also Section 2.5 below) to associate an individual with that person's Individual Information, and NutraThesis attempts to show advertising for products and services in which the person has expressed an interest in the Surveys, indicated an interest by means of Technology Information, and otherwise. NutraThesis may, at its discretion, target advertising by using email, direct mail, telephones, cell phones, and other means of communication to provide promotional offers.</p>

        <h3>2.4 Direct Mail and Telemarketing.</h3>

        <p>NutraThesis uses Individual Information to advertise, directly or indirectly, to individuals using direct mail marketing or telemarketing using telephones and cell phones.</p>

        <h3>2.5 Use of Technology Information.</h3>

        <p>NutraThesis uses Technology Information (1) to match a person's Survey Information and Third Party List Information to other categories of Individual Information to make and improve profiles of individuals, (2) to track a person's online browsing habits on the Internet, (3) to determine which areas of NutraThesis's web sites are most frequently visited. This information helps NutraThesis to better understand the online habits of individuals so that NutraThesis can target advertising and promotions to them.</p>

        <h3>2.6 Profiles of Individuals.</h3>

        <p>NutraThesis uses Individual Information to make a profile of an individual. A profile can be created by combining Survey Information and Third Party List Information with other sources of Individual Information such as information obtained from public databases.</p>

        <h3>2.7 Storage of Individual Information.</h3>

        <p>NutraThesis stores the Individual Information in a database on NutraThesis computers. Our computers have security measures (such as a firewall) in place to protect against the loss, misuse, and alteration of the information under NutraThesis's control. Not withstanding such measures, NutraThesis cannot guarantee that its security measures will prevent NutraThesis computers from being illegally accessed, and the Individual Information on them stolen or altered.</p>

        <h2>3. Order Fulfillment.</h2>

        <p>NutraThesis will transfer Individual Information to third parties when necessary to provide a product or service that a person orders from such third party while using NutraThesis web sites or when responding to offers provided by NutraThesis.</p>

        <h2>4. Legal Process.</h2>

        <p>NutraThesis may disclose Individual Information to respond to subpoenas, court orders, and other legal processes.</p>

        <h2>5. Summary Data.</h2>

        <p>NutraThesis may sell or transfer non-individualized information, such as summary or aggregated anonymous information about all persons or sub-groups of persons.</p>

        <h2>6 Access.</h2>

        <p>Individuals have access to their Individual Information collected to provide an opportunity for an individual to correct, amend, or delete such information. Access can be obtained by contacting customer service at the number on the order page. NutraThesis may also grant advertising clients and email services providers access to an individual's email address to verify the origin of the Individual Information collected.</p>

        <h2>7. Privacy Practices of Third Parties.</h2>

        <h3>7.1 Advertiser cookies and web beacons.</h3>

        <p>Advertising agencies, advertising networks, and other companies (together, "Advertisers") who place advertisements on the Websites and on the Internet generally may use their own cookies, web beacons, and other technology to collect information about individuals. NutraThesis does not control Advertisers' use of such technology and NutraThesis has no responsibility for the use of such technology to gather information about individuals.</p>

        <h3>7.2 Links.</h3>

        <p>The Websites and email messages sometimes contain hypertext links to the websites of third parties. NutraThesis is not responsible for the privacy practices or the content of such other web sites. Linked web sites may contain links to web sites maintained by third parties. Such links are provided for your convenience and reference only. NutraThesis does not operate or control in any respect any information, software, products or services available on such third party web sites. The inclusion of a link to a web site does not imply any endorsement of the services or the site, its contents, or its sponsoring organization.</p>

        <h2>8. Unsubscribe Procedures.</h2>

        <p>If you wish to discontinue receiving email messages from NutraThesis please just click on the unsubscribe link on the bottom of the email message. We rNutraThesisrve the right to add Individual Information to multiple lists maintained by NutraThesis.</p>

        <p>For more information about protecting your privacy, you may wish to visit: www.ftc.gov If you have questions about this policy, please feel free to contact us at the number on the order page.</p>
        <p>All Rights Reserved. * The NutraThesis statements have not been reviewed by the Food and Drug Administration. This site or products are not intended to diagnose, treat, cure, or prevent any disease.</p>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="rich-text">

    <p>
        For any questions, please contact us at support [at] nutrathesis.com<br>
        Copyright &copy; 1994-<?=date('Y')?> by NutraThesis.com. All Rights Reserved.<br>
        Our <a href="/privacy/">Privacy Policy</a> &nbsp;&amp;&nbsp;
        <a href="/terms/">Terms &amp; Conditions</a>.
    </p>

</div><!-- #footer -->

</body>
</html>
