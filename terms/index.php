<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <title>Terms and Conditions</title>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="web/s/reset.css">
    <link rel="stylesheet" type="text/css" href="web/s/zhtml.css">
    <link rel="stylesheet" type="text/css" href="web/s/global.css">
    <link rel="stylesheet" type="text/css" href="web/s/mobile.css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>

</head>
<body>

<div id="header" class="clearfix">

	<img class="mobile-half-width logo" src="web/i/head-logo.png" width="260px" alt="">

</div><!-- #header -->

<div id="layout" class="clearfix">

    <div id="content" class="rich-text clearfix">

	<h1>Terms and Conditions of Use / Service Agreement</h1>

    <p>This site provides its services to you subject to the Terms and Conditions contained in this Service Agreement. If you visit or shop at this site, you accept these Terms and Conditions.</p>
    <p>The material provided on NutraThesis is designed for educational and entertainment purposes only. It is provided with the understanding that this website is not engaged in rendering medical advice or recommendation.</p>
    <p>While we hope you find the Services useful, they are in no way intended to serve as a diagnostic service or platform, to provide certainty with respect to a diagnosis, to recommend a particular product or therapy or to otherwise substitute for the clinical judgment of a qualified healthcare professional.</p>
    <p>You should not rely on any information in text files, messages, or articles on this page to replace consultations with qualified health care professionals to meet your individual medical needs. Please consult a physician before beginning any exercise or diet program.</p>
    <p>Materials provided by NutraThesis are for personal use only. Permission to otherwise reprint or electronically reproduce any document in part or in its entirety is expressly prohibited, unless prior written consent is obtained from the rightful owner.</p>
    <p>The compilation of information on NutraThesis, including the design and organization is copyrighted and may not be reprinted nor electronically reproduced.</p>
    <p>If you do not agree to be bound by all of these Terms and Conditions, please do not use or access this Website. The contents of this Website, including the text, graphics, images, and information obtained from third-party content providers, sponsors, suppliers, and licensors (collectively “Providers”), and any other materials are to be used for informational purposes only.</p>
	
	<h3>PARENTAL OR GUARDIAN PERMISSION.</h3>
	
	<p>You must be 18 years or older to purchase any product or service offered on or through the Website. Some of the content on the Website may not be appropriate for children. CHILDREN UNDER THE AGE OF 13 ARE NOT PERMITTED TO USE THIS WEBSITE. We strongly recommend that children between the ages of 13 and 18 ask for their parent&#8217;s or guardian&#8217;s permission before viewing the Website.</p>
	
	<h3>PRIVACY.</h3>
	
	<p>Please review our <a href="../privacy">Privacy Policy</a>, which also governs your visit to the Website, to understand our privacy practices.</p>
	
	<h3>COMMUNICATIONS.</ph3
	
	<p>When you visit the Website or fill out any online forms, you are communicating with us electronically. You consent to receive communications (including legal notices) from us electronically. We will communicate with you by email, phone, text messages, mail or by posting notices on the Website. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing. We may also deliver communications to you electronically. In addition, we may deliver communications (including legal notices) to you at the street address you provided. Finally, we may deliver communications to you by any means set forth in any other policy or notice published on the Website.</p>
	
	<h3>LICENSE AND SITE ACCESS.</h3>
	
	<p>This website grants you a limited license to access and make personal use of the Website and you may not download (other than page caching) or modify it, or any portion of it, without the express written consent of the owners and authors. This license does not include any resale or commercial use of the Website or its contents; any collection and use of any product listings, descriptions, or prices; any derivative use of the Website or its contents; or any use of data mining, robots, or similar data gathering and extraction tools. The Website or any portion thereof may not be reproduced, duplicated, copied, sold, resold, or otherwise exploited for any commercial purpose without express written consent. You may not frame or utilize any framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of this website without our express written consent.  You are granted a limited, revocable, and nonexclusive right to create a hyperlink to the home page of the Website so long as the link does not portray This site, it’s owners or authors, its affiliates, or their products or services in a false, misleading, derogatory, or otherwise offensive manner. You may not use any proprietary graphics or trademarks as part of the link without express written permission.</p>
	
	<h3>CONTACT</h3>
	
	<p>You may contact us by filling out the contact us form on our website (please provide your first and last name, and order number if applicable)</p>
	
	<h3>DISCLAIMERS AND LIMITATION OF LIABILITY.</h3>
	
	<p>Users of the Website (individually and collectively, &#8220;User&#8221;) expressly agree that use of the Website is at User&#8217;s sole risk. Neither the owners, nor its employees or Providers, warrant that the Website will be uninterrupted or error-free; nor do they warrant or make any representation regarding the use of the information provided on the Website or the results that may be obtained from the use of the information provided on the Website, or as to the accuracy, reliability, or currency of any information, content, service, or merchandise provided through the Website. We do not endorse, recommend, or sponsor and are not affiliated with any individuals or entities listed or linked to on the Website unless that fact is expressly stated. The listing of any individual or entity does not constitute a medical referral of any kind. Users are advised to exercise their own further informed review, judgment, and evaluation in the selection of any and all medical professionals and health information. THE WEBSITE IS PROVIDED ON AN &#8220;AS IS&#8221; AND &#8220;AS AVAILABLE&#8221; BASIS AND MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE WEBSITE OR THE INFORMATION, CONTENT, MATERIALS, PRODUCTS OR INDIVIDUALS INCLUDED OR LISTED ON THE WEBSITE. TO THE FULLEST EXTENT PERMISSIBLE BY APPLICABLE LAW, WE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. UNDER NO CIRCUMSTANCES SHALL THIS SITE OR PROVIDERS BE LIABLE TO YOU OR ANY THIRD-PARTY FOR ANY INDIRECT, CONSEQUENTIAL, INCIDENTAL, SPECIAL OR PUNITIVE DAMAGES, INCLUDING, BUT NOT LIMITED TO, LOST PROFITS AND BUSINESS INTERRUPTION, WHETHER IN CONTRACT OR IN TORT, INCLUDING NEGLIGENCE, ARISING IN ANY WAY FROM ANY PRODUCT OR SERVICE SOLD, RECOMMENDED OR PROVIDED ON THE WEBSITE OR THE USE OF THE INFORMATION OR THE RESULTS OF THE USE OF THE INFORMATION PROVIDED ON THE WEBSITE, EVEN EXPRESSLY ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. IN NO EVENT SHALL THIS SITES LIABILITY EXCEED THE PRICE YOU PAID FOR A PRODUCT OR SERVICE THAT IS THE SUBJECT OF THE CLAIM. NO ORAL ADVICE OR WRITTEN INFORMATION GIVEN ON THIS SITE, BY ITS PROVIDERS OR THE LIKE, SHALL CREATE A WARRANTY; NOR SHALL USER RELY ON ANY SUCH INFORMATION OR ADVICE. Under no circumstances shall any party involved in creating, producing, or distributing this Website be liable for any direct, indirect, incidental, special, or consequential damages that result from the use of or inability to use the Website, including but not limited to the results from mistakes, omissions, interruptions, deletion of files or email, errors, defects, viruses, delays in operation or transmission, or any failure of performance, whether or not resulting from acts of God, communications failure, theft, destruction, or unauthorized access to the Website&#8217;s records, programs, or services. User acknowledges that this paragraph shall apply to all content, merchandise, and services available through the Website. In those states that do not allow the exclusion or limitation of liability for consequential or incidental damages, liability is limited to the fullest extent permitted by law.</p>
	
	<h3>THIRD-PARTY CONTENT.</h3>
	
	<p>The Website contains information, data, software, photographs, graphs, videos, typefaces, graphics, audio and other material (collectively &#8220;Content&#8221;). Regarding the Content supplied by Users or parties other than the owners of this site, This site is not a publisher or distributor, and has no more editorial control over such third-part Content or websites that we link to than does a public library or newsstand. Any opinions, advice, statements, services, offers, or other information that constitutes part of Content expressed or made available by third parties and not by this site are those of the respective authors or distributors. Neither  owners of this site nor any third party, including any Provider, or any User of the Website, guarantees the accuracy, completeness, or usefulness of any Content, nor its merchantability or fitness for any particular purpose. In many instances, the Content available through the Website represents the opinions and judgments of the respective Provider or User not under contract with this site. The owners of this site  neither endorse nor are responsible for the accuracy or reliability of any opinion, advice, or statement made on 3rd party Websites we may link to. Under no circumstances shall the owners of this site be liable for any loss, damage or harm caused by a User&#8217;s reliance on information obtained through the Website. It is the responsibility of a User to evaluate the information, opinion, advice or other Content available through the Website.</p>
	
	<h3>LINKS TO OTHER SITES.</h3>
	
	<p>The Website may reference or link to third-party sites throughout the World Wide Web. We have no control over these third-party sites or the content within them. We cannot and do not guarantee, represent or warrant that the content contained in these third-party sites is accurate, legal, or inoffensive. The owners of this site do not endorse the content of any third-party site, nor do we warrant that they will not contain viruses or otherwise impact your computer. We do not assume any responsibility or liability for the actions, product, services, and content of all these and any other third parties. If you choose to click thru to or use a third-party website, you should carefully review such third party&#8217;s privacy statement and other terms and conditions of use. By using the Website to click on any link to another third-party site, you agree and understand that you may not make any claim against this site or its owners for any damages or losses, whatsoever, resulting from your use of the Website to obtain information.</p>
	
	<h3>COPYRIGHTS.</h3>
	
	<p>Copyright &copy; NutraThesis.com 2010. All rights reserved. All materials and contents contained in the Website (including but not limited to the text, graphics, logos, button icons, images, audio clips, digital downloads, data compilations and software), and the Website itself, are copyrighted materials belonging exclusively to NutraThesis.com or its content suppliers and are protected by United States and international copyright law. NutraThesis.com enforces its copyright interests to the fullest extent permitted under the law, and shall seek civil and criminal remedies where appropriate, including the remedies provided for under sections 501 et seq. of Title 17 of the U.S. Code. All rights are reserved.</p>
	
	<h3>TRADEMARKS.</h3>
	
	<p>NutraThesis.com, and the related logos and other marks indicated on our Website are the exclusive property and trademarks of NutraThesis.com reserves all rights, including all rights applicable under the U.S. and international trademark laws, including, without limitation Section 1125 of Title 15 of the U.S. Code (Lanham Act Sec. 43). All other trademarks not owned by NutraThesis.com that appear on this Website are the property of their respective owners, who may or may not be affiliated with, connected to, or sponsored by NutraThesis.com .</p>
	
	<h3>INDEMNITY.</h3>
	
	<p>You agree to defend, indemnify, and hold NutraThesis.com and Providers harmless from and against any claims, actions or demands, liabilities and settlements including without limitation, reasonable legal and accounting fees, resulting from, or alleged to result from, your violation of these Terms and Conditions.</p>
	
	<h3>APPLICABLE LAW.</h3>
	
	<p>The Website was created and is controlled in the country of Canada, Province of Ontario. As such, the laws of the Province of Ontario will govern these disclaimers, terms and conditions, without giving effect to any principles of conflicts of laws. NutraThesis reserves the right to make changes to its Website and these disclaimers, terms and conditions at any time. User hereby irrevocably and unconditionally consents to submit to the jurisdiction of the Province of Ontario, Canada for any litigation arising out of or relating to use of or purchase made through the Website (and agrees not to commence any litigation relating thereto except in such courts), waives any objection to the laying of venue of any such litigation in the Canadian courts and agrees not to plead or claim in any Canadian court that such litigation brought therein has been brought in an inconvenient forum.</p>
	
	<h3>MISCELLANEOUS TERMS.</h3>
	
	<p>In any action against us arising from the use of this Website, the prevailing party shall be entitled to recover all legal expenses incurred in connection with the action, including but not limited to its costs, both taxable and non-taxable, and reasonable attorney&#8217;s fees. NutraThesis.com reserves the right to make changes to the Website, these policies, and these Terms and Conditions at any time, effective immediately upon the posting on this Website. Please check these Terms and Conditions periodically. In addition, these Terms &amp; Conditions may be modified only by our posting of changes to these Terms &amp; Conditions on this Website. Each time you access this Website, you will be deemed to have accepted any such changes. If any of these terms and conditions shall be deemed invalid, void, or for any reason unenforceable, that term or condition shall be deemed severable and shall not affect the validity and enforceability of any remaining terms and conditions. These Terms &amp; Conditions are the entire agreement between you and us relating to the subject matter herein. We may assign our rights and obligations under these Terms &amp; Conditions. These Terms &amp; Conditions will inure to the benefit of our successors, assigns, and licensees. The failure of either party to insist upon or enforce the strict performance of the other party with respect to any provision of these Terms &amp; Conditions, or to exercise any right under the Terms &amp; Conditions, will not be construed as a waiver or relinquishment to any extent of such party&#8217;s right to assert or rely upon any such provision or right in that or any other instance; rather, the same will be and remain in full force and effect.</p>
	
	<h3>Unsolicited Idea Submission Policy</h3>
	
	<p>NutraThesis.com, and/or any of its employees, do not accept or consider unsolicited ideas, including ideas for new advertising campaigns, new promotions, new or improved products or technologies, product enhancements, processes, materials, marketing plans or new product names. Please do not send any original creative artwork, suggestions, or other works. The sole purpose of this policy is to avoid potential misunderstandings or disputes when NutraThesis.com’s products or marketing strategies might seem similar to ideas submitted to NutraThesis.com. So, please do not send your unsolicited ideas to NutraThesis.com or anyone at NutraThesis.com. If, despite our request that you not send us your ideas, you still send them, then regardless of what your letter, fax, phone call, or e-mail says, the following terms shall apply to your idea submission.</p>
	
	<h3>Terms of Idea Submission</h3>
	
	<p>You agree that: (1) your ideas will automatically become the property of NutraThesis.com, without compensation to you, and (2) NutraThesis.com can use the ideas for any purpose and in any way, even give them to others.</p>
	
	<h3>Product and/or Service Feedback</h3>
	
	<p>NutraThesis.com does, however, welcome your feedback regarding many areas of NutraThesis.com’s existing business. If you want to send us your feedback, and we hope you do, we simply request that you fill out the form on the contact us page. Please provide only specific feedback on NutraThesis.com&#8217;s existing products or marketing strategies; do not include any ideas that NutraThesis.com’s policy will not permit it to accept or consider. It&#8217;s just one more way that NutraThesis.com can learn how to best satisfy your needs.</p>
	
	<h3>Feedback and Information</h3>
	
	<p>Any feedback you provide at this site shall be deemed to be non-confidential. NutraThesis.com shall be free to use such information on an unrestricted basis.</p>

    </div><!-- #content -->

</div><!-- #layout -->

<div id="footer" class="rich-text">

    <p>
        For any questions, please contact us at support [at] nutrathesis.com<br>
        Copyright &copy; 1994-<?=date('Y')?> by NutraThesis.com. All Rights Reserved.<br>
        Our <a href="/privacy/">Privacy Policy</a> &nbsp;&amp;&nbsp;
        <a href="/terms/">Terms &amp; Conditions</a>.
    </p>

</div><!-- #footer -->

</body>
</html>
